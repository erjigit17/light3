//
//  ViewController.swift
//  light3
//
//  Created by erjigit on 1/10/20.
//  Copyright © 2020 erjigit. All rights reserved.
//

import UIKit

enum Currentight {
    case red
    case yellow
    case green
}

class ViewController: UIViewController {
    
    @IBOutlet weak var redLight: UIImageView!
    @IBOutlet weak var yellowLight: UIImageView!
    @IBOutlet weak var greenLight: UIImageView!
    
    @IBOutlet weak var nextButtonPressed: UIButton!
    
    private var currentLight = Currentight.red

    private let lightOn: Float = 1.0
    private let lightOff: Float = 0.2
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        redLight.layer.opacity = lightOff
        yellowLight.layer.opacity = lightOff
        greenLight.layer.opacity = lightOff
        
    }

    @IBAction func nbp() {
        
        nextButtonPressed.setTitle("Next", for: .normal)
        
        switch currentLight {
        case .red:
            redLight.layer.opacity = lightOn
            greenLight.layer.opacity = lightOff
            currentLight = .yellow
        case .yellow:
            redLight.layer.opacity = lightOff
            yellowLight.layer.opacity = lightOn
            currentLight = .green
        case .green:
            greenLight.layer.opacity = lightOn
            yellowLight.layer.opacity = lightOff
            currentLight = .red
        }
    }
    
    
}

